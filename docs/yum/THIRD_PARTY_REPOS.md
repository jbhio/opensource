# Verified Repositories
These repositories have been verified by IBM as meeting general best practices for how the software is built. For instance, that the software uses the same cnventions for creating and linking to libraries, or that the software installs into appropriate locations on the filesystem. Existence on this list does not imply that all code has been verified by IBM, not does it imply any level of support unless otherwise stated. 

* <none yet! watch this space>

# Unverified Repositories
These repositories may offer IBM i software, but the software may not follow the same conventions as the IBM packages, may not be open source, etc. 

* <none yet! watch this space>